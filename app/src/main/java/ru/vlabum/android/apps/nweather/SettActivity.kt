package ru.vlabum.android.apps.nweather

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_sett.*
import kotlinx.android.synthetic.main.content_sett.*

class SettActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sett)
        setSupportActionBar(sett_toolbar)
        sett_city.setText(DataStorage.instance().city)
        sett_is_load_img.isChecked = App.instance?.isLoadImage!!
    }

    fun onClickApply(view: View) {
        App.instance?.isLoadImage = sett_is_load_img.isChecked
        DataStorage.instance().setCity(sett_city.text.toString())
        if (!sett_is_load_img.isChecked)
            App.instance?.imageView?.setImageResource(android.R.color.transparent)
        finish()
    }

}
