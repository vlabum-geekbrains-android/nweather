package ru.vlabum.android.apps.nweather

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import ru.vlabum.android.apps.nweather.dummy.CityContent
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity(),
    CityFragment.OnListFragmentInteractionListener,
    BlankFragment.OnFragmentInteractionListener {

    private val TAG = "t123"

    lateinit var cityFragment: CityFragment
    var blankFragment: BlankFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate(savedInstanceState)")
        super.onCreate(savedInstanceState)
        // доступ и ИНТЕРНЕТ
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        App.instance?.locale = resources.configuration.locale // TODO: добавить условие для выбора locales или locale
        App.instance!!.imageView = imageView1
        App.instance?.context = applicationContext

        DataStorage.instance().setCity("Chelyabinsk")
        DataStorage.instance().setLang(if ("ru".equals(App.instance!!.locale.toString())) "ru" else "en")

        initFragments()
        blankFragment?.updateView()
    }

    private fun initFragments() {
        cityFragment = supportFragmentManager.findFragmentById(R.id.city_fragment) as CityFragment
        if (supportFragmentManager.findFragmentById(R.id.fragment_blank_id) != null)
            blankFragment = supportFragmentManager.findFragmentById(R.id.fragment_blank_id) as BlankFragment
    }

    override fun onStart() {
        super.onStart()
        getWeather(DataStorage.TypeQuery.CURRENT)  // TODO: вынести в отдельный трэд
        getWeather(DataStorage.TypeQuery.FORECAST) // TODO: вынести в отдельный трэд
    }

    private fun getWeather(current: DataStorage.TypeQuery) {
        val url = DataStorage.instance().getRequestStringWeather(current)
        val obj = URL(url)
        val con = obj.openConnection() as HttpURLConnection

        con.requestMethod = "GET"

        try {
            BufferedReader(InputStreamReader(con.inputStream)).use {
                val response = StringBuffer()
                var inputLine = it.readLine()
                while (inputLine != null) {
                    response.append(inputLine)
                    inputLine = it.readLine()
                }
                DataStorage.instance().storeWeather(response.toString(), current)
            }
        } catch (e: Exception) {
            Toast.makeText(applicationContext, "Запрос погоды не удался", Toast.LENGTH_LONG).show()
            Log.d(TAG, e.toString())
        }
        con.disconnect()
    }

    override fun onFragmentInteraction(uri: Uri) {
        Log.d(TAG, "onFragmentInteraction")
    }

    override fun onListFragmentInteraction(item: CityContent.CityItem?) {
        Log.d(TAG, "onListFragmentInteraction")
        if (!updateBlankFragment(item)) {
            val intent = Intent(applicationContext, BlankFragmentActivity::class.java)
            intent.putExtra("content", item!!.content)
            startActivity(intent)
        }
    }

    private fun updateBlankFragment(item: CityContent.CityItem?): Boolean {
        if (blankFragment != null && blankFragment!!.isVisible) {
            DataStorage.instance().setCity(item!!.content)
            blankFragment!!.getWeather(DataStorage.TypeQuery.CURRENT)
            blankFragment!!.updateView()
            return true
        }
        return false
    }

    fun onClickSett(view: View) {
        val intent = Intent(applicationContext, SettActivity::class.java)
        startActivity(intent)
    }

}
