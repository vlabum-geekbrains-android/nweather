package ru.vlabum.android.apps.nweather

import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log

open class BlankFragmentActivity : AppCompatActivity(), BlankFragment.OnFragmentInteractionListener {

    val TAG = "t123"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_blank)
        val cityName = intent.getStringExtra("content") as String
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_blank_id) as BlankFragment?
        DataStorage.instance().setCity(cityName)
        fragment?.getWeather(DataStorage.TypeQuery.CURRENT)
        fragment?.updateView()
    }

    override fun onFragmentInteraction(uri: Uri) {
        Log.d(TAG, "onFragmentInteraction")
    }

}