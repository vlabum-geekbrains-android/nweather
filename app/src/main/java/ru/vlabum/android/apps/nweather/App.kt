package ru.vlabum.android.apps.nweather

import android.app.Application
import android.content.Context
import android.widget.ImageView
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import java.util.*


class App : Application() {

    companion object {
        var instance: App? = null
        fun instance(): App? {
            return instance
        }
    }

    var isLoadImage: Boolean = true
    var imageView: ImageView? = null
    lateinit var locale: Locale
    lateinit var context: Context

    lateinit var cicerone: Cicerone<Router>


    override fun onCreate() {
        super.onCreate()
        instance = this
        initCicerone()
    }

    private fun initCicerone() {
        cicerone = Cicerone.create()
    }

    fun getNavigatorHolder(): NavigatorHolder {
        return cicerone.navigatorHolder
    }

    fun getRouter(): Router {
        return cicerone.router
    }

}